.PHONY: all
.PHONY: clean

all: cxx03 cxx11 cxx11fix
clean:
	$(RM) cxx03 cxx11 cxx11fix

cxx03: cxx03.cxx
	$(CXX) $^ -o $@ -std=c++03
cxx11: cxx11.cxx
	$(CXX) $^ -o $@ -std=c++11
cxx11fix: cxx11fix.cxx
	$(CXX) $^ -o $@ -std=c++11
