#include <iostream>
#include <memory>
struct A {
  A(): p_(new int[3]) {std::cout << "[m] A()\n";}
  A(const A & rhs): p_(new int[3]) {std::cout << "[m] A(&)\n";}
  A(A &&) = default;
  ~A() = default;

  std::unique_ptr< int [] > p_;
};

A foo(const A & lhs, const A & rhs)
{
  return lhs;
}

A foo(A && lhs, const A & rhs)
{		//Let's "reuse"
  return lhs;	//already allocated object
}

A foo(A && lhs, A && rhs)
{		//Let's "reuse"
  return lhs;	//already allocated object
}

int main()
{
  A a0 = A();
  A a1 = A();

  A a2 = foo(a0, a1);

  //Still 2 allocs
  A a3 = foo(A(), a1);
  //Still 3 allocs
  A a4 = foo(A(), A());
  //Total 8 allocs
}
