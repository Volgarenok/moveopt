#include <iostream>
#include <memory>
struct A {
  A(): p_(new int[3]) {std::cout << "[m] A()\n";}
  A(const A & rhs): p_(new int[3]) {std::cout << "[m] A(&)\n";}
  ~A() {delete [] p_;};
   int * p_;
};

A foo(const A & lhs, const A & rhs)
{
  return lhs;
}

int main()
{
  //Only 1 alloc
  A a0 = A();
  A a1 = A();

  A a2 = foo(a0, a1);

  //Total 2 allocs
  A a3 = foo(A(), a1);
  //Allocs:  ^

  //Total 3 allocs
  A a4 = foo(A(), A());
  //Allocs:  ^    ^
  //Total 8 allocs
}
