#include <iostream>
#include <memory>
struct A {
  A(): p_(new int[3]) {std::cout << "[m] A()\n";}
  A(const A & rhs): p_(new int[3]) {std::cout << "[m] A(&)\n";}
  A(A &&) = default;
  ~A() = default;

  std::unique_ptr< int [] > p_;
};

A foo(const A & lhs, const A & rhs)
{
  return lhs;
}

A foo(A && lhs, const A & rhs)
{
  return std::move(lhs);//Forward object, let's reuse it
}

A foo(A && lhs, A && rhs)
{			//Can't reuse both
  return std::move(lhs);//Let's reuse one of them
}

int main()
{
  A a0 = A();
  A a1 = A();

  A a2 = foo(a0, a1);

  //Now 1 alloc
  A a3 = foo(A(), a1);
  //Now 2 allocs
  A a4 = foo(A(), A());
  //Total 6 allocs
}
